n = int(input())
p = int(input())
char = input()

def cria_tela(char, m, n):
    tela = [ [char for j in range(m)] for i in range(n)]
    return tela

def print_tela(tela):
    print('+' + '-'*len(tela[0]) + '+')
    for l in tela:
        print('|', end='')
        for e in l:
            print(e, end='')
        print('|')
    print('+' + '-'*len(tela[0]) + '+')

def reduce_triangle(tela, p, minx=0, maxx=None, miny=0, maxy=None, char=char):
    global printed_p
    if maxx == None:
        maxx = len(tela)
    if maxy == None:
        maxy = len(tela[0])

    w = (maxx - minx)
    h = (maxy - miny)
    for i in range(h//2):
        for j in range(w//4):
            tela[miny + i][minx+j] = ' '
            tela[miny + i][maxx-(j+1)] = ' '

    if p == 0:
        return 0
    else:
        reduce_triangle(tela, p-1, minx+w//2, minx+w, miny+h//2, miny+h)
        reduce_triangle(tela, p-1, minx, minx+w//2, miny+h//2, miny+h)
        reduce_triangle(tela, p-1, minx+w//4, minx+3*w//4, miny, miny+h//2)
        return 0

tela = cria_tela(char, 2**n, 2**n)
reduce_triangle(tela, p)
print_tela(tela)
