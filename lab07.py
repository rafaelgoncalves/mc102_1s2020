tipo = input()
c = input()
dim = int(input())

if tipo not in ['Q', 'T', 'L', 'O', 'H']:
    print('Identificador invalido.')
elif dim  < 3:
    print('Dimensao invalida.')
else:
    if tipo == 'Q':
        for _ in range(dim):
            for _ in range(dim):
                print(c, end='')
            print()
    elif tipo == 'T':
        max_ = dim*2 - 1
        for i in range(dim):
            print(' '*((max_ - (2*i + 1))//2), end='')
            print(c*(i*2+1))
    elif tipo == 'L':
        for i in range(dim-1):
            print(' '*(dim - (i+1)), end='')
            print(c*(2*i+1))
        print(c*(2*dim - 1))
        for i in range(dim-1)[::-1]:
            print(' '*(dim - (i+1)), end='')
            print(c*(2*i+1))
    elif tipo == 'H':
        for i in range(dim-1):
            print(' '*(dim - (i+1)), end='')
            print(c*(2*i + dim))
        print(c*(3*dim - 2))
        for i in range(dim-1)[::-1]:
            print(' '*(dim - (i+1)), end='')
            print(c*(2*i + dim))
    elif tipo == 'O':
        for i in range(dim-1):
            print(' '*(dim - (i+1)), end='')
            print(c*(2*i + dim))
        for i in range(dim):
            print(c*(3*dim - 2))
        for i in range(dim-1)[::-1]:
            print(' '*(dim - (i+1)), end='')
            print(c*(2*i + dim))
