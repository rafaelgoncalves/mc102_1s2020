m = float(input())
print(f'Massa corporal: {m:.1f}')
idade = int(input())
print(f'Idade: {idade}')
if (16 <= idade < 18) :
    doc = input()
    print(f'Documento de autorizacao: {doc}')
sint = input()
print(f'Febre ou sintomas gripais: {sint}')
viag = input()
print(f'Viagem recente ao exterior: {viag}')
cont = input()
print(f'Contato com caso de COVID-19: {cont}')
prim = input()
print(f'Primeira doacao: {prim}')
if (prim == 'N'):
    doac = int(input())
    print(f'Doacoes nos ultimos 12 meses: {doac}')
    if  doac:
        meses = int(input())
        print(f'Meses desde ultima doacao: {meses}')
sexo =  input()
print(f'Sexo biologico: {sexo}')
if (sexo == 'F'):
    gravidx = input()
    print(f'Gravida ou amamentando: {gravidx}')

autorizadx = True
if m < 50:
    print('Impedimento: abaixo da massa corporal minima')
    autorizadx = False
if idade < 16:
    print('Impedimento: menor de 16 anos')
    autorizadx = False
if (16 <= idade < 18) and (doc == 'N'):
    print('Impedimento: menor de 18 anos sem consentimento dos responsaveis')
    autorizadx = False
if (idade > 60) and (prim == 'S'):
    print('Impedimento: maior de 60 anos e primeira doacao')
    autorizadx = False
if idade > 69:
    print('Impedimento: maior de 69 anos')
    autorizadx = False
if sint == 'S':
    print('Impedimento: febre ou sintomas gripais')
    autorizadx = False
if viag == 'S':
    print('Impedimento: viagem recente ao exterior')
    autorizadx = False
if cont == 'S':
    print('Impedimento: contato com caso de COVID-19')
    autorizadx = False
if (prim == 'N'):
    if (sexo == 'M') and (doac >= 4):
        print('Impedimento: numero maximo de doacoes anuais foi atingido')
        autorizadx = False
    if (sexo == 'F') and (doac >= 3):
        print('Impedimento: numero maximo de doacoes anuais foi atingido')
        autorizadx = False
    if (sexo == 'M') and (doac >= 4) and (meses < 2):
        print('Impedimento: intervalo minimo entre as doacoes nao foi respeitado')
        autorizadx = False
    if (sexo == 'F') and (doac >= 3) and (meses < 3):
        print('Impedimento: intervalo minimo entre as doacoes nao foi respeitado')
        autorizadx = False
if (sexo == 'F') and (gravidx == 'S'):
    print('Impedimento: gravida ou amamentando')
    autorizadx = False

if autorizadx:
    print('Agende um horario para triagem completa')

