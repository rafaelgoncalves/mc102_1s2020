l1 = float(input())
l2 = float(input())
l3 = float(input())

a, b, c = sorted([l1, l2, l3])[::-1]

if (a <= 0 or b <= 0 or c <= 0 or a > b+c):
    print('Valores invalidos na entrada')
else:
    a2, b2, c2 = a**2, b**2, c**2
    if (a2 == b2 == c2):
        print('Triangulo equilatero')
    elif (a == b or b == c):
        print('Triangulo isosceles')
    else:
        print('Triangulo escaleno')
    if (a2 < b2 + c2):
        print('Triangulo acutangulo')
    elif (a2 == b2+c2):
        print('Triangulo retangulo')
    else:
        print('Triangulo obtusangulo')


