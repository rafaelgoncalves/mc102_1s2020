names = []
name = input()
while name != '--':
    names.append(name)
    name = input()

friends = {}
for e in sorted(names):
    friends.update({f'{e}': []})

pair = input()
while pair != '--':
    pair = pair.split()
    friends[pair[0]].append(pair[1])
    friends[pair[1]].append(pair[0])
    pair = input()

for i, e in enumerate(sorted(names)):
    for f in sorted(names)[i+1:]:
        print(e, f, ':', end='')
        print_comma = 0
        tmp = []
        for g in friends[e]:
            if g in friends[f]:
                tmp.append(g)
        for g in sorted(tmp):
            if print_comma:
                print(',', end='')
            print(f' {g}', end='')
            print_comma = 1
        print()

