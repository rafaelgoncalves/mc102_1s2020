entrada = input()

pal = []
num = []
has = []
emo = []

for e in entrada.split():
    if all([c.isalpha() for c in e]):
        pal.append(e)
    elif all([c.isdigit() for c in e]) or (e[0] == '-' and \
            all([c.isdigit() for c in e[1:]])):
        num.append(e)
    elif (e[0] == '#') and all([c.isalpha() for c in e[1:]]):
        has.append(e)
    else:
        emo.append(e)

cat = ['Palavra(s):', 'Numero(s):', 'Hashtag(s):', 'Emoticon(s):']
elems = [pal, num, has, emo]

for e, f in zip(cat, elems):
    print(e, end='')
    for g in f:
        print(f' {g}', end='')
    print()
