contador, contador_0, contador_1, contador_2 = 0, 0, 0, 0
t_medio = 0
v_max = 0
v_min = 1e10

t = int(input())
while (t > 0):

    t_medio += t
    contador += 1
    if t < 180:
        contador_0 += 1
    elif 180 <= t < 240:
        contador_1 += 1
    else:
        contador_2 += 1

    v = 60*33.0/t
    if v > v_max:
        v_max = v
    if v < v_min:
        v_min = v

    t = int(input())

print('Caracois no nivel 0:', contador_0)
print('Caracois no nivel 1:', contador_1)
print('Caracois no nivel 2:', contador_2)
print(f'Tempo medio: {(float(t_medio)/contador):.1f} s')
print(f'Velocidade maxima: {v_max:.1f} cm/min')
print(f'Velocidade minima: {v_min:.1f} cm/min')
