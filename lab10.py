def print_game(game):
    print('+'+('-'*(len(game[0])-2))+'+')
    for row in game[1:-1]:
        print('|', end='')
        for e in row[1:-1]:
            print('@' if e else ' ', end='')
        print('|')
    print('+'+('-'*(len(game[0])-2))+'+')

def read_game():
    game = []
    stop_string = input()
    row = stop_string
    cont = 1
    while cont:
        row_list = []
        for e in row:
            if (e == '@'):
                row_list.append(1)
            else:
                row_list.append(0)
        game.append(row_list)
        row = input()
        if row == stop_string:
            cont = 0
    game.append([0]*len(row))
    return game

def state(game, i, j):
    cell = game[i][j]

    cells_nb = 0
    for ki in range(-1, 2):
        for kj in range(-1, 2):
            cells_nb += game[i+ki][j+kj]
    cells_nb -= cell

    if cell and (2 <= cells_nb <= 3):
        return 1
    elif cells_nb > 3:
        return 0
    elif cells_nb < 2:
        return 0
    elif not cell and cells_nb == 3:
        return 1
    else:
        return 0

def game_step(game):
    new_game = [x[:] for x in game]
    for i in range(1, len(game)-1):
        for j in range(1, len(game[0])-1):
            new_game[i][j] = state(game, i, j)
    return new_game

def main():
    game = read_game()
    iter_nb = int(input())
    for i in range(iter_nb):
        print_game(game)
        game = game_step(game)
    print_game(game)

main()
