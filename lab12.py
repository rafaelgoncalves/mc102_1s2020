def print_list(l):
    print('.'*(len(l)+2))
    for i in range(max(l), 0, -1):
        print('.', end='')
        for e in l:
            print('|' if (e>=i) else ' ', end='')
        print('.')
    print('.'*(len(l)+2))

def bubble_sort(l):
    swap = True
    i = 0
    print_list(l)
    while swap:
        swap = False
        for i, (e, f) in enumerate(zip(l, l[1:])):
            if e > f:
                l[i], l[i+1] = f, e
                swap = True
                print_list(l)
    return l

l = [int(e) for e in input().split()]
bubble_sort(l)
