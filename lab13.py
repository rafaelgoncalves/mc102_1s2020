def print_list(pad, max_val, l, idx):
    for i, _ in enumerate(l[:max_val]):
        if i >= pad: 
            print('+', end='')
            print('-'*5 if i != idx else '='*5, end='')
        else:
            print(' '*6, end='')
    print('+')

    for i, _ in enumerate(l[:max_val]):
        if i >= pad:
            print(f"|{' ' if i != idx else '|'}{l[i]:03d}{' ' if i != idx else '|'}", end='')
        else:
            print(' '*6, end='')
    print('|')

    for i, _ in enumerate(l[:max_val]):
        if i >= pad:
            print('+', end='')
            print('-'*5 if i != idx else '='*5, end='')
        else:
            print(' '*6, end='')
    print('+')

def binary_search(a, x, lo=0, hi=None):
    pad = 0
    max_val = len(a)
    if hi is None:
        hi = len(a)
    while lo < hi:
        mid = (lo+hi)//2
        midval = a[mid]
        print_list(pad, max_val, a, mid)
        if midval < x:
            lo = mid+1
            pad = mid+1
        elif midval > x: 
            hi = mid
            max_val = mid
        else:
            return mid
    return -1

def main():
    x = int(input())
    a = [int(e) for e in input().split()]

    print('Elemento procurado:', str(x).zfill(3))
    if a != sorted(a):
        print("Lista nao ordenada")
        return -1
    ans = binary_search(a, x)
    if ans == -1:
        print("O elemento nao foi encontrado")
        return -2
    else:
        print("Encontrado na posicao:", ans)

main()

