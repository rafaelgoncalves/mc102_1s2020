
def tupla_float_int(x) :
    x = x[1:-1]       # remove parenteses
    x = x.split(",")  # separa em duas strings
    f = float(x[0])   # converte primeiro elemento para float
    i = int(x[1])     # converte segundo elemento para int
    return (f,i)      # retorna tupla

notas_lab = [tupla_float_int(x) for x in input().split()]
prova1, prova2 = [float(x) for x in input().split()]

ml = 0
acc = 0
for nota, peso in notas_lab:
    ml += nota*peso
    acc += peso
ml /= acc
print(f'Media das tarefas de laboratorio: {ml:.1f}')
mp = (prova1*3 + prova2*4)/7.0
print(f'Media das provas: {mp:.1f}')
if (ml >= 5) and (mp >= 5):
    mfinal = 0.7*mp  + 0.3*ml
elif (mp >= 2.5) and (ml >= 2.5):
    mprelim = min(4.9, 0.7*mp + 0.3*ml)
    print(f'Media preliminar: {mprelim:.1f}')
    exame = float(input())
    print(f'Nota no exame: {exame:.1f}')
    mfinal = (mprelim + exame)/2
else:
    mfinal = min(mp, ml)
if (mfinal >= 5):
    print("Aprovado(a) por nota e frequencia")
else:
    print("Reprovado(a) por nota")
print(f"Media final: {mfinal:.1f}")
